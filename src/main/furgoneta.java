package main;
import java.math.BigDecimal;

public class furgoneta extends automovil{
	Double volumendecarga;
	
public furgoneta () {
	

	 this.numllantas = 4;
	 
	 this.setMarca("Hyundai");
	 
	 this.setModelo("2010");
	 
	 this.precio = new BigDecimal("78950000"); 
	 
	 this.volumendecarga= 4.5;
}

public Double getVolumendecarga() {
	return volumendecarga;
}

public void setVolumendecarga(Double volumendecarga) {
	this.volumendecarga = volumendecarga;
}

}
